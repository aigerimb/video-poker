const getRandomValue = (arr) => {
  const value = Math.floor(Math.random()*arr.length);
  return value;
};

const RANKS = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"];
const SUITS = ["♠", "♥", "♦", "♣"];
const WORTH = {"2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7, "8": 8, "9": 9, "10": 10, "J": 11, "Q": 12, "K": 13, "A": 14};

class Card {
  constructor() {
    console.log("Карта была создана!")
  };

  createCard() {
    this.card = {};
    const rankIndex = getRandomValue(RANKS);
    const suitIndex = getRandomValue(SUITS);
    this.card.rank = RANKS[rankIndex];
    this.card.suit = SUITS[suitIndex];
    return this.card;
  };

  printCard() {
    console.log(this.createCard());
  };
}

class Deck {
  constructor() {
    console.log("Создаем колоду!")
  }

  createDeck() {
    this.deck = [];
    for (let rank of RANKS) {
      for (let suit of SUITS) {
        const newCard = {};
        newCard.rank = rank;
        newCard.suit = suit;
        if (newCard.rank >= 2 || newCard.rank <= 10) {
          newCard.worth = parseInt(rank);
        } else {
          if (newCard.rank === "J") newCard.worth = 11;
          else if (newCard.rank === "Q") newCard.worth = 12;
          else if (newCard.rank === "K") newCard.worth = 13;
          else newCard.worth = 14;
        }
        this.deck.push(newCard);
      }
    }
    return this.deck;
  };

  getCardFromDeck() {
    const randomIndex = getRandomValue(this.deck);
    const randomCard = this.deck[randomIndex];
    this.deck.splice(randomIndex, 1);
    return randomCard;
  };

  shuffleDeck() {
    this.deck.map((item, index, arr) => {
      let randomIndex = getRandomValue(arr);
      arr[index] = arr[randomIndex];
      arr[randomIndex] = item;
    })
    return this.deck;
  };

}

class Hand {
  constructor() {
    console.log("Игрок в деле");
  }

  getCards(cards) {
    this.cards = cards;
  };

  showCards() {
    console.log("Карты игрока:")
    for (let card of this.cards) {
      console.log(`${this.cards.indexOf(card)+1}: `, card);
    }
  };

  replaceCard(index, card) {
    this.cards[index-1] = card;
  };

}

class Checker {
  constructor() {
    console.log("Проверяющий готов")
  }

  getSuitAmount(cards) {
    this.suitAmount = {};

    for (let suit of SUITS) {
      this.suitAmount[suit] = 0;
    }

    for (let key in this.suitAmount) {
      for (let card of cards) {
        if (key === card.suit) {
          this.suitAmount[key] += 1;
        }
      }
    }
    return this.suitAmount;
  };

  getRankAmount(cards) {
    this.rankAmount = {};

    for (let rank of RANKS) {
      this.rankAmount[rank] = 0;
    }

    for (let key in this.rankAmount) {
      for (let card of cards) {
        if (key === card.rank) {
          this.rankAmount[key] += 1;
        }
      }
    }
    return this.rankAmount;
  };

  getWorthList(cards) {
     this.worthList = cards.map((item) => {
      return item.worth;
    })
    return this.worthList;
  };

  checkCertainSuitAmount() {
    const suitAmountList = Object.values(this.suitAmount);

    const isDifferentSuit = suitAmountList.every( item => {
      return item >= 0 && item < 5;
    });

    const isSameSuit = () => {
      let isTrue = false;
      for (let key in this.suitAmount) {
        if (this.suitAmount.key === 5) isTrue = true;
      }
      return isTrue;
    }

    this.worthList.sort((a, b) => a-b);

    const isRankInOrder = this.worthList.every((value, index, arr) => {
      let diff = 1;
      const nextIndex = index + 1;
      if (nextIndex !== arr.length) {
        diff = arr[nextIndex] - value;
      }
      return diff === 1;
    });



    const isAllHighCards = () => {
      let worthSum = 0;
      for (let i of this.worthList) {
        worthSum+=i;
      }
      if (worthSum === 60) return true;
    };

    this.points = 0;

    if (isDifferentSuit && isRankInOrder) {
      this.points += 5;
      console.log("Стрит (straight) - 5 карт разных мастей по порядку - 5 очка");
    } else if (isSameSuit()) {
      this.points += 6;
      console.log("Флеш (flush) - 5 любых карт одной масти - 6 очков");
    } else if (!isDifferentSuit && isRankInOrder) {
      this.points += 9;
      console.log("Стрит-флеш (straight flush) - 5 любых других карт одной масти по порядку - 9 очков");
    } else if (!isDifferentSuit && isAllHighCards && isRankInOrder) {
      this.points += 10;
      console.log("Роял-флеш (royal flush) - 5 старших карт одной масти по порядку - 10 очков");
    }
  };

  checkCertainRankAmount() {
    this.worthList.sort((a, b) => a - b);
    const lastRank = this.worthList[4];
    let pairSum = 0;
    let tripleSum = 0;
    for (let key in this.rankAmount) {
       if (this.rankAmount[key] === 2) {
        pairSum++;
      } else if (this.rankAmount[key] === 3) {
        tripleSum++;
      } else if (this.rankAmount[key] === 4) {
         this.points += 8;
        console.log("Каре/четвёрка (four of a kind) - 8 очков");
      } else if (this.rankAmount[key] === 1) {
        if (WORTH[key] === lastRank) {
          this.points += 1;
          console.log(`Старшая карта (high card): ${key} - 1 очко`);
        }
      }
    }

    if (pairSum === 2) {
      this.points += 3;
      console.log("Две пары (two pairs) - 3 очка");
    } else if (pairSum === 1) {
      if (tripleSum === 1) {
        this.points += 7;
        console.log("Фулл-хаус (full house) - 7 очков");
      }
      else {
        this.points += 2;
        console.log("Пара (pair) - 2 очка");
      }
    } else if (tripleSum === 1) {
      this.points += 4;
      console.log("Тройка (three of a kind) - 4 очка");
    }
  };

}

class Application {
  constructor() {
    this.deck = new Deck();
    this.hand = new Hand();
    this.checker = new Checker();
    console.log("Игра началась!\n----------------------------------");
  }

  startGame() {
    this.deck.createDeck();
    this.deck.shuffleDeck();

    this.cards = [];
    for (let i = 0; i < 5; i++) {
      this.cards.push(this.deck.getCardFromDeck());
    }

    this.hand.getCards(this.cards);
    this.hand.showCards();

    const answer = confirm("Хотите заменить карты?");

    if (answer) {
      const indexes = prompt("Введите порядковый номер, если их несколько введите через пробел");
      indexes.split(" ");
      console.log(`Заменяем карту под номером: ${indexes}`);
      for (let index of indexes) {
        this.hand.replaceCard(parseInt(index), this.deck.getCardFromDeck());
      }
    } else {
      console.log("Хорошо, ничего не меняем");
    }

    this.hand.showCards();
    console.log(this.deck);
    this.checker.getSuitAmount(this.cards);
    this.checker.getRankAmount(this.cards);
    this.checker.getWorthList(this.cards);
    this.checker.checkCertainSuitAmount();
    this.checker.checkCertainRankAmount();
    console.log(`Ваши очки: ${this.checker.points}\n----------------------------------\n `);
  };
}
